import { Component, OnInit } from '@angular/core';
import { CropInfo } from './../../classes/crop-info';
import { ApiService } from './api.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'ooraja-totalcrops',
  templateUrl: './totalcrops.component.html',
  styleUrls: ['./totalcrops.component.scss']
})
export class TotalcropsComponent implements OnInit {
 
  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();
  
  cropModel:CropInfo[];

  constructor(private apiService:ApiService) { }
 

  ngOnInit(): void {
    this.getAllCropDetails();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true
    };
  }

  
  getAllCropDetails() {
    this.apiService.getData().subscribe((a) => {
      this.cropModel = a as CropInfo[];
      
      this.dtTrigger.next();
      
    });
  }
}
