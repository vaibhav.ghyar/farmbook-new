import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getData() {
    return this.http
      .get<any>(`${environment.apiUrl}/show/cropDetails`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }
}