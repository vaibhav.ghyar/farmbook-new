import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalcropsComponent } from './totalcrops.component';

describe('TotalcropsComponent', () => {
  let component: TotalcropsComponent;
  let fixture: ComponentFixture<TotalcropsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalcropsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalcropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
