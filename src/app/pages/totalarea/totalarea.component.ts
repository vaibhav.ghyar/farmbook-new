
import { ApiService } from './api.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'ooraja-totalarea',
  templateUrl: './totalarea.component.html',
  styleUrls: ['./totalarea.component.scss']
})
export class TotalareaComponent implements OnInit {
  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  areaModel:any;

  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.getAllAreaDetails();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 7,
      processing: true
    };
  }

  getAllAreaDetails() {
    this.apiService.getData().subscribe((a:any) => {
      this.areaModel = a;
      this.dtTrigger.next();
      
    });
  }
}
