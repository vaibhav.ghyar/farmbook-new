import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalareaComponent } from './totalarea.component';

describe('TotalareaComponent', () => {
  let component: TotalareaComponent;
  let fixture: ComponentFixture<TotalareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
