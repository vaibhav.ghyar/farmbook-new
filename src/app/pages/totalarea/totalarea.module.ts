import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TotalareaComponent } from './totalarea.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [TotalareaComponent],
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild([
      {
        path: '',
        component: TotalareaComponent,
      },
    ]),
  ],
})
export class TotalareaModule { }
