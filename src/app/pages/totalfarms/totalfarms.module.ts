import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TotalfarmsComponent } from './totalfarms.component';


@NgModule({
  declarations: [ TotalfarmsComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: TotalfarmsComponent,
      },
    ]),
  ],
})
export class TotalfarmsModule { }
