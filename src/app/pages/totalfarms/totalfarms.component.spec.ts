import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalfarmsComponent } from './totalfarms.component';

describe('TotalfarmsComponent', () => {
  let component: TotalfarmsComponent;
  let fixture: ComponentFixture<TotalfarmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalfarmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalfarmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
