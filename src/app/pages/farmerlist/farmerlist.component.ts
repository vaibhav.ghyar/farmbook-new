import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { City } from './../../classes/city';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Farmer } from './../../classes/farmer';
import { ApiService } from './api.service';
import { FarmerlistModel } from './farmerlist.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'ooraja-farmerlist',
  providers: [ApiService],
  templateUrl: './farmerlist.component.html',
  styleUrls: ['./farmerlist.component.scss'],
})

export class FarmerlistComponent implements OnInit {
  farmerlistModelObj: FarmerlistModel = new FarmerlistModel();
  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();
  
  city: City[];
  gender: any = ['male', 'female'];
  farmers: FarmerlistModel[];
  currentCity: City;

  formValue!: FormGroup;
  showAdd!: boolean;
  showUpdate!: boolean;
  showUpdateTitle!: boolean;
  showAddTitle!: boolean;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {
    this.formValue = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      cityName: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      contact: ['', [ Validators.required]],
      altcontact: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getCityList();
    this.getAllFarmerlist();
    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
    
   
    
  }

  addButtonClickFunction() {
    this.formValue.reset();
    this.showUpdate = false;
    this.showAdd = true;
    this.showUpdateTitle = false;
    this.showAddTitle = true;
  }

  getCityList() {
    return this.api.getCityData().subscribe((a) => {
      this.city = a;
    });
  }

  postFarmerlistDetails() {
    this.farmerlistModelObj.firstName = this.formValue.value.firstName;
    this.farmerlistModelObj.lastName = this.formValue.value.lastName;
    this.farmerlistModelObj.gender = this.formValue.value.gender;
    this.farmerlistModelObj.cityId = this.formValue.value.cityName;
    this.farmerlistModelObj.phoneNo = this.formValue.value.contact;
    this.farmerlistModelObj.alternatePhoneNo = this.formValue.value.altcontact;

    this.farmerlistModelObj.userTypeId = 1;

    let cancel = document.getElementById('cancel');
    this.api.postData(this.farmerlistModelObj).subscribe((a) => {
      alert('Record inserted successfully');
      cancel?.click();
      this.formValue.reset();
      this.getAllFarmerlist();
    });

    this.dtTrigger.next();
  }

  getAllFarmerlist() {
    this.api.getData().subscribe((a) => {
      this.farmers = a as FarmerlistModel[];
     this.formValue.reset();
     
    });
  
  }

  deleteFarmerlist(row: any) {
    if (confirm('Are you sure? want to delete this User') === true) {
      this.api.deleteData(row.id).subscribe((a) => {
        this.getAllFarmerlist();
        this.formValue.reset()
       
       
      });
    }
  }
  
  updateFarmerlist(row: any) {
    this.showUpdate = true;
    this.showAdd = false;

    this.showUpdateTitle = true;
    this.showAddTitle = false;
    this.farmerlistModelObj.id = row.id;
    this.formValue.controls['firstName'].setValue(row.firstName);
    this.formValue.controls['lastName'].setValue(row.lastName);
    this.formValue.controls['gender'].setValue(row.gender);
    this.formValue.controls['cityName'].setValue(row.cityId);
    this.formValue.controls['contact'].setValue(row.phoneNo);
    this.formValue.controls['altcontact'].setValue(row.alternatePhoneNo);
  }

  updateFarmerlistDetails() {
    this.farmerlistModelObj.firstName = this.formValue.value.firstName;
    this.farmerlistModelObj.lastName = this.formValue.value.lastName;
    this.farmerlistModelObj.gender = this.formValue.value.gender;
    this.farmerlistModelObj.cityId = this.formValue.value.cityName;
    this.farmerlistModelObj.phoneNo = this.formValue.value.contact;
    this.farmerlistModelObj.alternatePhoneNo = this.formValue.value.altcontact;
    this.farmerlistModelObj.userTypeId = 1;

    this.api.updateData(this.farmerlistModelObj).subscribe((a) => {
      alert('Record updated Succesfully');

      let cancel = document.getElementById('cancel');

      this.formValue.reset();
      this.getAllFarmerlist();
      this.dtTrigger.next();
    });
  }

  changeActive(id:any,values:any){
    let status:any;
    if (values.currentTarget.checked){
      status = 1;
    }else{
      status = 0;
      
    }
  
    this.api.ChangeActiveStatus(id,status).subscribe((a) => {
      this.getAllFarmerlist();
    });
  }

}
