import { FarmerlistModel } from './farmerlist.model';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  postData(data: any) {
    return this.http
      .post<any>(`${environment.apiUrl}/farmbook/user/register`, data)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getData() {
    return this.http
      .get<any>(`${environment.apiUrl}/farmbook/user/getByTypeId/1`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getCityDataById(id: number) {
    let typeId = String(id);
    return this.http
      .get<any>(`${environment.apiUrl}/city/getCityById/${typeId}`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  updateData(data: FarmerlistModel) {
    let userId = String(data.id);
    return this.http
      .put<any>(
        `${environment.apiUrl}/farmbook/user/updateUser/${userId}`,
        data
      )
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  deleteData(id: number) {
    let userId = String(id);
    return this.http
      .delete<any>(`${environment.apiUrl}/farmbook/user/removeUser/${userId}`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  ChangeActiveStatus(id: number,status:number) {
    let data ={
          "id":id,
          "activeStatus":status}
    return this.http
      .put<any>(`${environment.apiUrl}/farmbook/user/changeUserActiveStatus`, {"id":id,"activeStatus":status})
      .pipe(
        map((res: any) => {
          return res;
        })
      );

  }

  getCityData() {
    return this.http
      .get<any>(`${environment.apiUrl}/city/getAllCities`, {
        headers: { skip: 'true' },
      })
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }
}
