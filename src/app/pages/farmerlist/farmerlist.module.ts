import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FarmerlistComponent } from './farmerlist.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [FarmerlistComponent],
  imports: [
    DataTablesModule,
    CommonModule,
    ReactiveFormsModule,
    
  
    RouterModule.forChild([
      {
        path: '',
        component: FarmerlistComponent,
      },
    ]),
  ],
})
export class FarmerlistModule {}
