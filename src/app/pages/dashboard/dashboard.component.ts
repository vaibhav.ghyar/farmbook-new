import { ApiService } from './api.service';
import { FarmerlistComponent } from './../farmerlist/farmerlist.component';
import { Component, OnInit } from '@angular/core';
declare let google:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  farmersByCity:any;
  totalFarmerData:any;
  totalLandData:any;
  totalFarmsData:any;
  totalActiveFarmerData:any
   constructor(private apiService:ApiService) {
  }
// 
  getCityWiseFarmerData() {
      this.apiService.getFarmersByCity().subscribe((a) => {
      //  console.log(a);
       
      this.farmersByCity = a; 
      // console.log(this.farmersByCity);
      
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(this.drawChart);
    });
  }
// 
  getTotalFarmerData() {
    this.apiService.gettotalFarmerData().subscribe((a:any) => {
      this.totalFarmerData = a;
    });
  }

  getTotalLandData() {
    this.apiService.getTotalLandData().subscribe((a:any) => {
      this.totalLandData = a;
     
    });
  }

  getTotalFarmsData() {
    this.apiService.getTotalFarmsData().subscribe((a:any) => {
      this.totalFarmsData = a;
      
    });
  }

  getTotalActiveFarmerData() {
    this.apiService.getTotalActiveFarmerData().subscribe((a:any) => {
      this.totalActiveFarmerData = a;
  
    });
  }

  drawChart() {   
  //  console.log(this.farmersByCity);

      // let data = google.visualization.arrayToDataTable(this.farmersByCity);


        //static data from api 
    
    var data = google.visualization.arrayToDataTable([
      [
        "City",
        "Farmers"
      ],
      [
        "Jalgaon",
        2
      ],
      [
        "Pune",
        4
      ],
      [
        "Mumbai",
        3
      ],
      [
        "Sangli",
        1
      ],
      [
        "Kolhapur",
        0
      ],
      [
        "Dhule",
        2
      ]
    ]);

    let options = {
      title: 'City Wise Farmers',
      pieHole: 0.4,
      'height' : 320,
      'width' : 400
    };

    let chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }

  ngOnInit() {
    this.getCityWiseFarmerData();
    this.getTotalFarmerData();
    this.getTotalLandData();
    this.getTotalFarmsData();
    this.getTotalActiveFarmerData();
    console.log(this.farmersByCity);
    
    }

    
    }

