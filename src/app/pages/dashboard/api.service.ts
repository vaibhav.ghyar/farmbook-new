import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  gettotalFarmerData() {
    return this.http
      .get<any>(`${environment.apiUrl}/show/totalFarmers`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getTotalLandData() {
    return this.http
      .get<any>(`${environment.apiUrl}/show/totalLand`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }
  
  getTotalFarmsData() {
    return this.http
      .get<any>(`${environment.apiUrl}/show/totalFarms`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getTotalActiveFarmerData() {
    return this.http
      .get<any>(`${environment.apiUrl}/show/totalActiveFarmers`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getFarmersByCity() {
    return this.http
      .get<any>(`${environment.apiUrl}/show/farmersByCity`)
      .pipe(
        map(data => {
          return data;
        })
      );
  }
}
