import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  postData(data: any) {
    console.log(data);
    return this.http
      .post<any>(`${environment.apiUrl}/emergency/addNewType`, data)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getData() {
    return this.http
      .get<any>(`${environment.apiUrl}/emergency/getAllEmergencyTypes`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  updateData(data: any) {
    let typeId = String(data.emergencyTypeId);
    return this.http
      .put<any>(`${environment.apiUrl}/emergency/updateType/${typeId}`, data)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  deleteData(id: number) {
    let typeId = String(id);
    console.log(`${environment.apiUrl}/emergency/removeType/${typeId}`);
    return this.http
      .delete<any>(`${environment.apiUrl}/emergency/removeType/${typeId}`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  ChangeActiveStatus(id: number,status:number) {
    let data ={
          "emergencyTypeId":id,
          "activeStatus":status}
    return this.http
      .put<any>(`${environment.apiUrl}/emergency/changeActiveStatus`, {"emergencyTypeId":id,"activeStatus":status})
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  
  }



}
