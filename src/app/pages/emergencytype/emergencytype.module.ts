import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { EmergencytypeComponent } from './emergencytype.component';
import { PrimngModuleModule } from '../../core/primng-module/primng-module.module';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [EmergencytypeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DataTablesModule,
    PrimngModuleModule,
    RouterModule.forChild([
      {
        path: '',
        component: EmergencytypeComponent,
      },
    ]),
  ],
})
export class EmergencytypeModule {}
