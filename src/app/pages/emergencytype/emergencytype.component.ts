import { EmergencyType } from './../../classes/emergency-type';
import { EmergencyTypeModel } from './emergencytype.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/pages/emergencytype/api.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'ooraja-emergencytype',
  providers: [ApiService],
  templateUrl: './emergencytype.component.html',
  styleUrls: ['./emergencytype.component.scss'],
})
export class EmergencytypeComponent implements OnInit {
  emergencytypeModelObj: EmergencyType; emergencyType: EmergencyType[];

  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  formValue!: FormGroup;
  showAdd!: boolean;
  showUpdate!: boolean;
  showUpdateTitle!: boolean;
  showAddTitle!: boolean;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {
    this.formValue = this.formBuilder.group({
      emergencyTypeName: ['', [Validators.required]],
    });
  }

  addButtonClickFunction() {
    this.formValue.reset();
    this.showUpdate = false;
    this.showAdd = true;
    this.showUpdateTitle = false;
    this.showAddTitle = true;
  }

  ngOnInit(): void {
    this.getAllEmergencytype();
    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
  }

  postEmergencytypeDetails() {
    this.emergencytypeModelObj = this.formValue.value;
    console.log(this.formValue.value);
    this.emergencytypeModelObj.emergencyTypeName =
      this.formValue.value.emergencyTypeName;

    let cancel = document.getElementById('cancel');
    this.api.postData(this.emergencytypeModelObj).subscribe((a) => {
      console.log(a);
      cancel?.click();
      this.formValue.reset();
      this.getAllEmergencytype();
    });
  }

  getAllEmergencytype() {
    this.api.getData().subscribe((a) => {
      this.emergencyType = a;

      this.dtTrigger.next();
    });
  }
  deleteEmergencytype(id: any) {
    if (confirm('Are you sure? want to delete this Emergency Type') === true) {
      this.api.deleteData(id).subscribe((a) => {
        this.getAllEmergencytype();
      });
    }
  }
  updateEmergencyType(data: EmergencyType) {
    this.showUpdate = true;
    this.showAdd = false;

    this.showUpdateTitle = true;
    this.showAddTitle = false;
    this.emergencytypeModelObj = data;
    this.formValue.controls['emergencyTypeName'].setValue(
      data.emergencyTypeName
    );
  }

  updateEmergencytypeDetails() {
    this.emergencytypeModelObj.emergencyTypeName =
      this.formValue.value.emergencyTypeName;

    this.api.updateData(this.emergencytypeModelObj).subscribe((a) => {
      alert('Record updated Succesfully');
      this.formValue.reset();
      this.getAllEmergencytype();
    });
  }

  changeActive(id:any,values:any){
    let status:any;
    if (values.currentTarget.checked){
      status = 1;
    }else{
      status = 0;
      
    }
   
    this.api.ChangeActiveStatus(id,status).subscribe((a) => {
      this.getAllEmergencytype();
    });
  }

}
