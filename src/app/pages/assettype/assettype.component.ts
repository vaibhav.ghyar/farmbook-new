import { Component, OnInit } from '@angular/core';
import { Assettype } from './assettype.model';
import { Subject } from 'rxjs';
import { AssetType } from './../../classes/asset-type';
import { ApiService } from 'src/app/pages/assettype/api.service';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'ooraja-assettype',
  templateUrl: './assettype.component.html',
  styleUrls: ['./assettype.component.scss']
})
export class AssettypeComponent implements OnInit {
  assettypeModelObj: AssetType;
  assettype: AssetType[];
 
  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  formValue!: FormGroup;
  showAdd!: boolean;
  showUpdate!: boolean;
  showUpdateTitle!: boolean;
  showAddTitle!: boolean;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {
    this.formValue = this.formBuilder.group({
      assetTypeName: [''],
    });
  }

  addButtonClickFunction() {
    this.formValue.reset();
    this.showUpdate = false;
    this.showAdd = true;
    this.showUpdateTitle = false;
    this.showAddTitle = true;
  }

  ngOnInit(): void {
    this.getAllAssettype();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
  }

  postAssetTypeDetails() {
    this.assettypeModelObj = this.formValue.value;
    this.assettypeModelObj.assetTypeName =
      this.formValue.value.assetTypeName;

    let cancel = document.getElementById('cancel');
    this.api.postData(this.assettypeModelObj).subscribe((a) => {
      cancel?.click();
      this.formValue.reset();
      this.getAllAssettype();
    });
  }

  getAllAssettype() {
    this.api.getData().subscribe((a) => {
      this.assettype = a;

      this.dtTrigger.next();
    });
  }
  deleteAssetType(id: any) {
    if (confirm('Are you sure? want to delete this Asset Type') === true) {
      this.api.deleteData(id).subscribe((a) => {
        this.getAllAssettype();
      });
    }
  }
  updateAssetType(data: AssetType) {
    this.showUpdate = true;
    this.showAdd = false;

    this.showUpdateTitle = true;
    this.showAddTitle = false;
    this.assettypeModelObj = data;
    this.formValue.controls['assetTypeName'].setValue(
      data.assetTypeName
    );
  }

  updateAssetTypeDetails() {
    this.assettypeModelObj.assetTypeName =
      this.formValue.value.assetTypeName;

    this.api.updateData(this.assettypeModelObj).subscribe((a) => {
      alert('Record updated Succesfully');
      this.formValue.reset();
      this.getAllAssettype();
    });
  }

  changeActive(id:any,values:any){
    let status:any;
    if (values.currentTarget.checked){
      status = 1;
    }else{
      status = 0;
    }
    this.api.ChangeActiveStatus(id,status).subscribe((a) => {
      this.getAllAssettype();
    });
  }
}

