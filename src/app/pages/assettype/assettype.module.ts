import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'; 
import { AssettypeComponent } from './assettype.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [AssettypeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DataTablesModule,
    RouterModule.forChild([
      {
        path: '',
        component: AssettypeComponent,
      },
    ]),
    
    
  ]
})
export class AssettypeModule { }
