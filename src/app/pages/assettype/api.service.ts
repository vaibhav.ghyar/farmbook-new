import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  postData(data: any) {
    console.log(data);
    return this.http
      .post<any>(`${environment.apiUrl}/assetType/addNewType`, data)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  getData() {
    return this.http
      .get<any>(`${environment.apiUrl}/assetType/getAllTypes`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  updateData(data: any) {
    let typeId = String(data.assetTypeId);
    return this.http
      .put<any>(`${environment.apiUrl}/assetType/updateType/${typeId}`, data)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  deleteData(id: number) {
    let typeId = String(id);
    return this.http
      .delete<any>(`${environment.apiUrl}/assetType/removeType/${typeId}`)
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  ChangeActiveStatus(id: number,status:number) {
    let data ={
          "assetTypeId":id,
          "activeStatus":status}
    return this.http
      .put<any>(`${environment.apiUrl}/assetType/changeActiveStatus`, {"assetTypeId":id,"activeStatus":status})
      .pipe(
        map((res: any) => {
          console.log(res);
          return res;
        })
      );
  
  }
}