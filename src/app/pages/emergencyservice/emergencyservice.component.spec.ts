import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyserviceComponent } from './emergencyservice.component';

describe('EmergencyserviceComponent', () => {
  let component: EmergencyserviceComponent;
  let fixture: ComponentFixture<EmergencyserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmergencyserviceComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
