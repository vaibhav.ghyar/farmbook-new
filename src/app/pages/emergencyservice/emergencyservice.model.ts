export class EmergencyServiceModel {
 
  
  typeName:string;
  emergencyServiceName: string = '';
  emergencyServiceAddress: string = '';
  emergencyServiceId: number;
  cityName:string;
  emergencyServiceContact: string = '';
  alternateEmergencyServiceContact: string = '';
  emergencyServiceTypeId: number;
  emergencyServiceCityId: number;
  activeStatus: number;
  emergencyServiceCityName:string;
  emergencyServiceTypeName:string;

}
