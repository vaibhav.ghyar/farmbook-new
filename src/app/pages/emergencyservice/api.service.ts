import { environment } from 'src/environments/environment';
import { AuthService } from './../../modules/auth/services/auth.service';
import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http'
import {map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiService { 

 constructor(private http:HttpClient, private_authService:AuthService) { }


getData(){
    return this.http.get<any>(`${environment.apiUrl}/emergency/getAllServices`).pipe(map((res:any)=>{
     return res;
  })) 
}
  
postData(data:any){
  console.log(data);
  
  return this.http.post<any>(`${environment.apiUrl}/emergency/addService`,data).pipe(map((res:any)=>{;
    return res;
}))
}

updateData(data:any){
  let serviceId = String(data.emergencyServiceId)
  return this.http.put<any>(`${environment.apiUrl}/emergency/updateService/${serviceId}`,data).pipe(map((res:any)=>{
   return res;
}))
}

deleteData(id:number){
  let serviceId = String(id)
  return this.http.delete<any>(`${environment.apiUrl}/emergency/removeService/${serviceId}`).pipe(map((res:any)=>{
   return res;
}))
}

getCityData() {
  return this.http
    .get<any>(`${environment.apiUrl}/city/getAllCities`, {
      headers: { skip: 'true' },
    })
    .pipe(
      map((res: any) => {
        return res;
      })
    );
}

getTypeData() {
  return this.http
    .get<any>(`${environment.apiUrl}/emergency/getAllTypes`)
    .pipe(
      map((res: any) => {
        return res;
      })
    );
}


ChangeActiveStatus(id: number,status:number) {
  let data ={
        "emergencyServiceId":id,
        "activeStatus":status}
  return this.http
    .put<any>(`${environment.apiUrl}/emergency/changeServiceActiveStatus`, {"emergencyServiceId":id,"activeStatus":status})
    .pipe(
      map((res: any) => {
        return res;
      })
    );

}


}
