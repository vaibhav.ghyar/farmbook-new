import { City } from './../../classes/city';
import { EmergencyType } from './../../classes/emergency-type';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup ,Validators } from '@angular/forms';
import { ApiService } from 'src/app/pages/emergencyservice/api.service';
import { EmergencyServiceModel } from './emergencyservice.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'ooraja-emergencyservice',
  templateUrl: './emergencyservice.component.html',
  styleUrls: ['./emergencyservice.component.scss'],
})

export class EmergencyserviceComponent implements OnInit {
  emergencyserviceModelObj: EmergencyServiceModel = new EmergencyServiceModel();
  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();


  emergencyServiceList: EmergencyServiceModel[];
  city: City[];
  currentCity: City;
  emergencyType: EmergencyType[];
  currentType: EmergencyType;

  formValue!: FormGroup;
  showAdd!: boolean;
  showUpdate!: boolean;
  showUpdateTitle!: boolean;
  showAddTitle!: boolean;

  constructor(private formBuilder: FormBuilder, private api: ApiService) {
    this.formValue = this.formBuilder.group({
      emergencyServiceName: ['',[Validators.required]],
      emergencyServiceAddress: [''],
      cityName: ['',[Validators.required]],
      typeName: ['',[Validators.required]],
      emergencyServiceContact: ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      alternateEmergencyServiceContact: ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    });
  }

  getCityList() {
    return this.api.getCityData().subscribe((a) => {
      this.city = a;
    });
  }

  getTypeList() {
    return this.api.getTypeData().subscribe((a) => {
      this.emergencyType = a;
    });
  }

  addButtonClickFunction() {
    this.formValue.reset();
    this.showUpdate = false;
    this.showAdd = true;
    this.showUpdateTitle = false;
    this.showAddTitle = true;
  }

  ngOnInit(): void {
    this.getCityList();
    this.getTypeList();
    this.getAllEmergencyservice();
   
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
  }

  postEmergencyserviceDetails() {
    this.emergencyserviceModelObj.emergencyServiceId =
      this.formValue.value.emergencyServiceId;
    this.emergencyserviceModelObj.emergencyServiceName =
      this.formValue.value.emergencyServiceName;
    this.emergencyserviceModelObj.emergencyServiceTypeId =
      this.formValue.value.typeName;
    this.emergencyserviceModelObj.emergencyServiceAddress =
      this.formValue.value.emergencyServiceAddress;
    this.emergencyserviceModelObj.emergencyServiceCityId =
      this.formValue.value.cityName;
    this.emergencyserviceModelObj.emergencyServiceContact =
      this.formValue.value.emergencyServiceContact;
    this.emergencyserviceModelObj.alternateEmergencyServiceContact =
      this.formValue.value.alternateEmergencyServiceContact;

    let cancel = document.getElementById('cancel');
    this.api.postData(this.emergencyserviceModelObj).subscribe((a) => {
      alert('Record inserted successfully');
      cancel?.click();
      this.formValue.reset();
      this.getAllEmergencyservice();
    });
  }

  getAllEmergencyservice() {
    this.api.getData().subscribe((a) => {
      this.emergencyServiceList = a as EmergencyServiceModel[];

      this.dtTrigger.next();
    });
  }

  deleteEmergencyservice(row: any) {
    if (
      confirm('Are you sure? want to delete this Emergency Service') === true
    ) {
      this.api.deleteData(row.emergencyServiceId).subscribe((a) => {
        this.getAllEmergencyservice();
      });
    }
  }

  updateEmergencyservice(row: any) {
    this.showUpdate = true;
    this.showAdd = false;

    this.showUpdateTitle = true;
    this.showAddTitle = false;
    this.emergencyserviceModelObj.emergencyServiceId = row.emergencyServiceId;
    this.formValue.controls['emergencyServiceName'].setValue(
      row.emergencyServiceName
    );
    this.formValue.controls['emergencyServiceAddress'].setValue(
      row.emergencyServiceAddress
    );
    this.formValue.controls['cityName'].setValue(
      row.emergencyServiceCityId
    );
    this.formValue.controls['typeName'].setValue(
      row.emergencyServiceTypeId
    );
    this.formValue.controls['emergencyServiceContact'].setValue(
      row.emergencyServiceContact
    );
    this.formValue.controls['alternateEmergencyServiceContact'].setValue(
      row.alternateEmergencyServiceContact
    );
  }

  updateEmergencyserviceDetails() {
    console.log(this.formValue.value.typeName);

    this.emergencyserviceModelObj.emergencyServiceName =
      this.formValue.value.emergencyServiceName;
    this.emergencyserviceModelObj.emergencyServiceTypeId =
      this.formValue.value.typeName;
    this.emergencyserviceModelObj.emergencyServiceAddress =
      this.formValue.value.emergencyServiceAddress;
    this.emergencyserviceModelObj.emergencyServiceCityId =
      this.formValue.value.cityName;
    this.emergencyserviceModelObj.emergencyServiceContact =
      this.formValue.value.emergencyServiceContact;
    this.emergencyserviceModelObj.alternateEmergencyServiceContact =
      this.formValue.value.alternateEmergencyServiceContact;

    this.api.updateData(this.emergencyserviceModelObj).subscribe((a) => {
      alert('Record updated Succesfully');

      this.formValue.reset();
      this.getAllEmergencyservice();
    });
  }

  changeActive(id:any,values:any){
    let status:any;
    if (values.currentTarget.checked){
      status = 1;
    }else{
      status = 0;
      
    }
    
    this.api.ChangeActiveStatus(id,status).subscribe((a) => {
      this.getAllEmergencyservice();
    });
  }
}















  // setCityName() {
  //   for (let index = 0; index < this.emergencyServiceList.length; index++) {
  //     this.currentCity = this.city.find((c) => {
  //       return c.cityId == this.emergencyServiceList[index].emergencyServiceCityId;
  //     })!;      
  //     this.emergencyServiceList[index].emergencyServiceCityName = this.currentCity.cityName;
  //   }
  // }

  // setTypeName() {
  //   for (let index = 0; index < this.emergencyServiceList.length; index++) {
  //     this.currentType = this.emergencyType.find((t) => {   
  //       return t.emergencyTypeId == this.emergencyServiceList[index].emergencyServiceTypeId;
  //     })!;
  //     this.emergencyServiceList[index].emergencyServiceTypeName = this.currentType.emergencyTypeName;
  //   }
  // }