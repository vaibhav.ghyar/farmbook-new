import { Routes } from '@angular/router';

const Routing: Routes = [
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
  },

  {
    path: 'farmerlist',
    loadChildren: () =>
      import('./farmerlist/farmerlist.module').then((m) => m.FarmerlistModule),
  },

  {
    path: 'emergencytype',
    loadChildren: () =>
      import('./emergencytype/emergencytype.module').then(
        (m) => m.EmergencytypeModule
      ),
  },

  {
    path: 'emergencyservice',
    loadChildren: () =>
      import('./emergencyservice/emergencyservice.module').then(
        (m) => m.EmergencyserviceModule
      ),
  },

  {
    path: 'totalfarms',
    loadChildren: () =>
      import('./totalfarms/totalfarms.module').then((m) => m.TotalfarmsModule),
  },
  {
    path: 'totalarea',
    loadChildren: () =>
      import('./totalarea/totalarea.module').then((m) => m.TotalareaModule),
  },

  {
    path: 'totalcrops',
    loadChildren: () =>
      import('./totalcrops/totalcrops.module').then((m) => m.TotalcropsModule),
  },

  {
    path: 'assettype',
    loadChildren: () =>
      import('./assettype/assettype.module').then((m) => m.AssettypeModule),
  },

  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },

  {
    path: '**',
    redirectTo: 'error/404',
  },
];

export { Routing };
