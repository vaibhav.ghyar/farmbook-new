export class Farmer {
    id:number;
    firstName:string='';
    lastName:string;
    phoneNo:string;
    alternatePhoneNo:string;
    cityId:number;
}
