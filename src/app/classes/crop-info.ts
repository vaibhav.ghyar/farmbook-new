export class CropInfo {
    cropTypeId:number;
    cropTypeName:string;
    totalLand:number;
    farmCount:number;
}
