import { Router } from '@angular/router';
// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../modules/auth/services/auth.service';

@Injectable()
export class InterceptService implements HttpInterceptor {
	// urlsToNotUse: Array<string>;
	constructor(private router: Router, private authServcie: AuthService) { 

	}
	// intercept request and add token
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		const user = this.authServcie.currentUserValue;


		if (user) {
			const token = user.jwtToken;
			if (!request.headers.get("skip")) {
				console.log("inside request cloner")
				request = request.clone({
					setHeaders: {
						Authorization: `Bearer ${token}`
					}
				});
			 }
		} else {
			request = request.clone();
		}
		return next.handle(request).pipe(
			tap(
				event => {
					
					if (event instanceof HttpResponse) {
						console.log('all looks good');
						//when unautherize user then redirec to login page
						if(event.status===403){
							//this.openSnackBar(event.body.message,'')
							localStorage.clear();
							this.router.navigate(['farmbook/admin/adminLogin']);					
						}
						

					}
			
				},
				error => {
					// http response status code
					// console.log('----response----');
					// console.error('status code:');
					// tslint:disable-next-line:no-debugger
					console.error(error.status);
					console.error(error.message);
					if (error.status === 422) {
						this.openSnackBar(error.error.error, '');
					}

					// navigate to login and clear localstorage if the token is expired or
					// unauthorized error is occured from the backend
					if (error.status === 403) {
						localStorage.clear();
						// this.store.dispatch(new Logout());
						this.router.navigate(['farmbook/admin/adminLogin']);
					}
					// console.log('--- end of response---');
				}
			)
		);
	}

	// private isValidRequestForInterceptor(requestUrl: string): boolean {
	// 	let positionIndicator: string = 'api/';
	// 	let position = requestUrl.indexOf(positionIndicator);
	// 	if (position > 0) {
	// 	  let destination: string = requestUrl.substr(position + positionIndicator.length);
	// 	  for (let address of this.urlsToNotUse) {
	// 		if (new RegExp(address).test(destination)) {
	// 		  return false;
	// 		}
	// 	  }
	// 	}
	// 	return true;
	//   }
	openSnackBar(message: string, action: string) {
		// this.snackBar.open(message, action, {
		// 	duration: 2000,
		// });
	}
}
